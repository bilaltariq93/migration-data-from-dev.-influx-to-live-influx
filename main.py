from influxdb import InfluxDBClient
from datetime import datetime, time


def InsertData(jsondata, client):
    client.write_points(jsondata, database="livedb")


def JsonFormatForInflux(id, type, validity, time, default):
    json_body = [
        {
            "measurement": "none",
            "tags": {
                "id": id,
                "type": type,
                "validity": validity
            },
            "time": time,
            "fields": {
                "default": default,
            }
        }
    ]
    return json_body

def InfluxDBConnectionLive():
    host = ''
    port = 
    user = ''
    password = ''
    dbname = ''
    client = InfluxDBClient(host, port, user, password, dbname)
    return client


def InfluxDBConnectionDev():
    host = ''
    port = 
    dbname = ''
    client = InfluxDBClient(host, port, dbname)
    return client


def main():
    #Get data based on your Ids
    id = ''

    masterCount = 0
    query = ("SELECT * FROM "'"''none''"''' + " WHERE id = '"'' + id + ''''''"' ORDER BY time asc")

    clientSource = InfluxDBConnectionDev()

    clientDestination = InfluxDBConnectionLive()

    results = clientSource.query(query, database="")

    for r in results.get_points():
        masterCount += 1
    print("Total Data: " + id + " : " + str(masterCount))

    jsondata = []
    totalCount = 0
    start = datetime.now()
    pStart = datetime.now()
    jsondata_reset = False

    for row in results.get_points():
        totalCount = totalCount + 1

        if totalCount == 1 or jsondata_reset == False:
            jsondata = JsonFormatForInflux(row['id'], row['type'], (row['validity']), row['time'],
                                           row['default'] / 1000)
            jsondata_reset = True
        else:
            jsondata = jsondata + JsonFormatForInflux(row['id'], row['type'], row['validity'], row['time'],
                                                      row['default'] / 1000)

        if totalCount % 10000 == 0:
            InsertData(jsondata, clientDestination)
            t = datetime.now() - start
            start = datetime.now()
            print("Data Inserted..." + str(totalCount) + " : " + str(t))
            jsondata_reset = False
            jsondata = []

        if masterCount == totalCount:
            print("Data Inserted (Remindar)..." + str(masterCount % 10000))
            InsertData(jsondata, clientDestination)
            jsondata_reset = False
            jsondata = []

    print("\n" + id + " : " + str(totalCount) + "\nTotal Time:" + str(datetime.now() - pStart))


if __name__ == "__main__":
    main()
